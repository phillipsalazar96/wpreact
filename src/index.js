import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/App';
import './../assets/assets/app.scss';


window.addEventListener("DOMContentLoaded", function (e) {
    ReactDOM.render(<App />, document.getElementById('app'))
});

