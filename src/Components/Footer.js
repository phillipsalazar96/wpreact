import React from 'react';

const Footer = () => {
    const title = "testing";
    const year = (new Date()).getFullYear()
    
    return (
        <footer>
            <div className='container'>
                <div className='row'>
                    <div className='col-12'>
                    <p>{title} &copy; {year}</p>
                    </div>
                </div>
            </div>

        </footer>
    )
    
}

export default Footer;