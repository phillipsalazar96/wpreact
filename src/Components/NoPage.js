import React from 'react';

const NoPage = () => {
    return(
        <>
            <div className='row'>
                <div className='col-12'>
                    <p>
                       Page Can not be Found...
                    </p>
                </div>
            </div>
        </>
    )
}
export default NoPage;
