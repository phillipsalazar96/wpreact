import React, { useState } from 'react';
import useWPFetch from '../Objects/useWPFetch';
import Sidebar from './Sidebar';

const Content = () => {

    const postsUrl = 'posts';

    const [data] = useWPFetch(postsUrl);
    const [posts, setPosts] = useState(null);



    return(
        <div className='content-area'>
            <div className='container'>
                <div className='row'>
                    <div className='col-8'>
                            {posts &&
                            posts.map((post) => {
                            return <p key={post.id}>{post.title.rendered}</p>;
                        })}
                    </div>
                    <div className='col-4'>
                        <Sidebar />
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Content;