import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Content from './Content';
import Home from './Home';
import NoPage from './NoPage';
import { BrowserRouter, Routes, Route } from "react-router-dom";

const App = () => {
    
        return (
            <>
             <BrowserRouter>
                <Header/>
                <Content />  
                <Home />
                <Footer/>

                <Routes>
                    <Route index element={<Home />} />
                    <Route path="*" element={<NoPage />} />
                  </Routes>
              </BrowserRouter>
            </>
        );
    
}

export default App;