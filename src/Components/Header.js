import React from 'react';
import { Link } from "react-router-dom";
import useWPFetch from '../Objects/useWPFetch';

const Header = () => {

    const title = "Testing Site";

    return (
        <header>
            <div className="container">
                <div className="row">
                    <div className="col-8">
                    <h1>{title}</h1>
                    </div>
                    <div className="col-4">
                    <nav>
                        <Link to="/">Home</Link>
                        <a href="">Articles</a>
                        <a href="">About Us</a>
                    </nav>
                    </div>
                </div>


            </div>


        </header>
    )
    
}

export default Header;