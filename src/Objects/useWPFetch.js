import React, { useState, useEffect } from 'react';

const useWPFetch = (url = '') => {
    let urls = 'http://wordpresssites.ddev.site/wp-json/wp/v2/' + url;
    const [data, setData] = useState(null);

    useEffect(() => {
      fetch(urls,{
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
         }
      })
      //setData(data) 
        .then((res) => res.json())
        .then((data) => setData(data));
    }, [urls]);
  
    return [data];
}

export default useWPFetch;